
function pizzaLog(logtext){

    console.info("pizzaLog : "+logtext);
    var debugspan = document.getElementById('pizza-debug');
    if (!debugspan){
        var span = document.createElement('li');
        span.style.color = 'gray'; // apply your style
        span.style["text-align"] = 'right';
        span.style.width = '10em';
        span.id = "pizza-debug";
        span.appendChild(document.createTextNode(logtext));
        document.getElementById('oneHeader').querySelector('ul.slds-global-actions').prepend(span);
    }
    else debugspan.textContent = logtext;

}

function clearPizzaLog(){
    document.getElementById('pizza-debug').textContent = "";
}

function getCaseID(){
    if (document.URL.includes("Case") && document.title.includes("|")) {
        return document.title.split(' | ')[0];
    }
    else {
        return null;
    }
}

/* ------------ LOCAL STORAGE FUNCTIONS ------------ */
function pullFromStorage(caseID=getCaseID()){
    // pull from local storage and add to current global variables
    if (!window.caseData) window.caseData = {};
    if (!window.caseData[caseID]) window.caseData[caseID] = {};

    var storedCaseData = JSON.parse(localStorage.getItem("case"+caseID)) || {};

    for (const entry of Object.entries(storedCaseData)){
        window.caseData[caseID][entry[0]] = entry[1];
    }
}

function pushToStorage(caseID=getCaseID()){
    // overwrite local storage with global variables
    var storedCaseData = JSON.parse(localStorage.getItem("case"+caseID)) || {};

    for (const entry of Object.entries(window.caseData[caseID])){
        storedCaseData[entry[0]] = entry[1];
    }

    localStorage.setItem("case"+caseID, JSON.stringify(storedCaseData));
    updateMetadata(caseID)
}

function getCaseValue(key, caseID = getCaseID()){

    console.debug("Getting '"+key+"' from case "+caseID);
    
    if (!window.caseData || !window.caseData[caseID] || !window.caseData[caseID][key]){
        // can't find it in global variables
        pullFromStorage(caseID);
        // that should also set up window.caseData if it doesn't exist
    }

    return window.caseData[caseID][key] || null;

}

function setCaseValue(key,value, caseID = getCaseID()){
    // okay! so in the new version, we need to store all data in one string.
    // sync window.caseData with json stringify localstorage

    if (!window.caseData) window.caseData = {};
    if (!window.caseData[caseID]) window.caseData[caseID] = {};

    if (caseID){

        pullFromStorage(caseID);

        // Now add new key to global variables
        window.caseData[caseID][key] = value;

        // Now overwrite local storage with global variables
        pushToStorage(caseID);

    }
    else {
        window.caseData[caseID][key] = value;

    }
}

function updateMetadata(caseID = getCaseID()){
    // in this one we basically just log that the case was edited as well as its status
    var metadata = JSON.parse(localStorage.getItem("PizzaCaseMetadata") || "{}");

    // Format: {"3040596":{"status":"Closed"; "lastUpdated": date}}
    if (!metadata[caseID]) metadata[caseID] = {};

    metadata[caseID]["status"] = getCaseValue("status", caseID);
    metadata[caseID]["lastUpdated"] = Date.now();

    // now wrap it back up and store

    localStorage.setItem("PizzaCaseMetadata", JSON.stringify(metadata));
}

function clearAllClosedCases(){
    var metadata = JSON.parse(localStorage.getItem("PizzaCaseMetadata"));
    var newMetadata = {};

    for (c of Object.keys(metadata)){
        if (metadata[c].status === "Closed"){
            // got one, let's wipe it
            localStorage.removeItem("case"+c);

        } else { 
            // this one shouldn't be deleted, save it
            newMetadata[c] = metadata[c];
        }
    }

    localStorage.setItem("PizzaCaseMetadata", JSON.stringify(newMetadata));
}